/*
################################################################
# ViewController - Javascript-File
# Author: 		Matthias Rosenthal
# Website:		http://www.matthiasrosenthal.de
# Version 1.0:	2013/01/19
################################################################
*/

//init elements on window load complete
$(window).load(function() {

	//#########  init header-plugin -> override slide-duration-options
	$(".header").headerMenu({
		slideDownDuration : 0.6,
		slideUpDuration : 0.2
	});

	//#########  init product-plugin -> override slide-duration-options
	$(".product-slider").productSlider({
		slideDuration : 0.5
	});

	//#########  init primary-button-plugin
	$(".primary-button").primaryButton();
});
