/*
 ################################################################
 # Primary Button - Jquerry-Plugin-File overall for all views
 # Author: 		Matthias Rosenthal
 # Website:		http://www.matthiasrosenthal.de
 # Version 1.0:	2013/02/09
 ################################################################
 */
(function($) {
	$.fn.primaryButton = function() {
		
		//find each primary button
		return this.each(function() {
			//mouse events
			$(this).mouseover(function() {
				//roll over effect for dynamic button
				$(this).find(".button-left").css("background-position", "0 -42px");
				$(this).find(".button-mid").css("background-position", "0 -42px");
				$(this).find(".button-right").css("background-position", "0 -42px");
				$(this).css("cursor", "pointer");
			});
		
			$(this).mouseout(function() {
				//roll out effect for dynamic button
				$(this).find(".button-left").css("background-position", "0 0");
				$(this).find(".button-mid").css("background-position", "0 0");
				$(this).find(".button-right").css("background-position", "0 0");
			});
		});
	}
})(jQuery);