/*
 ################################################################
 # HeaderMenu - Jquery-Plugin-File overall for all pages
 # Author: 		Matthias Rosenthal
 # Website:		http://www.matthiasrosenthal.de
 # Version 1.0:	2013/02/07
 ################################################################
 */
(function($) {
	$.fn.headerMenu = function(options) {
		//set up default options
		var defaults = {
			dropDownHeight : 'auto', //the default width of drop down elements
			slideDownEasing : Cubic.easeOut, //easing method for slideDown
			slideUpEasing : Cubic.easeOut, //easing method for slideUp
			slideDownDuration : 0.5, //easing duration for slideDown
			slideUpDuration : 0.5 //easing duration for slideUp
		};

		//vars options
		var opts = $.extend({}, defaults, options);
		var currentID = "";

		return this.each(function() {
			//mouse over/out events of navigation-menu items
			$(this).find(".header-nav li").mouseover(function() {
				//add hover class
				var idName = $(this).attr("id");
				highlightMenuPoint(idName, true);
			});
			$(this).find(".header-nav li").mouseout(function() {
				//remove hover class
				var idName = $(this).attr("id");
				highlightMenuPoint(idName, false);
			});
			//click event of navigation-menu items
			$(this).find(".header-nav li").click(function() {
				//send offest-left for positionate the subnav-layer (plus 2xborderwith of 4px, plus 9px icon width, plus padding icon)
				var menuPointOffsetLeft = $(this).offset().left + $(this).width() + 8 + 9 + 5;
				//sending the current id for showing the right subnav-layer
				var idName = $(this).attr("id");
				toggleSubMenu(idName, menuPointOffsetLeft);
			});
		});
		//show Submenu
		function toggleSubMenu(idName, offsetLeft) {
			//set subnavigation-id
			var subNavIdName = "subnav_" + idName;

			if (currentID != idName) {
				//set overall-id
				currentID = idName;

				//reset all subnavs
				$(".header-subnav-container").css("height", "0px");
				$(".header-subnav-container").css("z-index", "0");
				$(".header-subnav-container").css("padding", "0px");
				$(".header-subnav-container").css("border", "0px solid #cfcfcf");
				$(".arrow-icon").css("background-position", "0 0");
				TweenMax.killTweensOf($(".header-subnav-container"));

				//set back all menu-item hover states -> set current menu-point
				$(".header-nav li").removeClass("nav-hover");
				$("#" + idName).addClass("nav-hover");

				//set arrow icon up / down
				$("#" + idName).find(".arrow-icon").css("background-position", "0 -9px");

				//set subnav to the middle proportional to the subnav-width
				var subNavWidth = $('#' + subNavIdName).width();
				var leftPos = ($(window).innerWidth() - subNavWidth) / 2;

				//check if subnav is not pending on nav-menu-point
				if (leftPos + subNavWidth < offsetLeft) {
					leftPos += offsetLeft - (leftPos + subNavWidth);
				}

				$('#' + subNavIdName).css("left", leftPos + "px");

				//show border
				$('#' + subNavIdName).css("border", "6px solid #cfcfcf");
				$('#' + subNavIdName).css("z-index", "2");
				$('#' + subNavIdName).css("padding", "10px");

				//read height of menu if dropDownHeight isn't set by the option-setting
				var headerHeightAuto;
				if (opts.dropDownHeight == 'auto') {
					$('#' + subNavIdName).css("height", "auto");
					headerHeightAuto = $('#' + subNavIdName).height();
				} else {
					headerHeightAuto = opts.dropDownHeight;
				}
				//set height to 0 on start
				$('#' + subNavIdName).css("height", 0);

				//animate slide down
				TweenMax.to($('#' + subNavIdName), opts.slideDownDuration, {
					css : {
						height : headerHeightAuto
					},
					ease : opts.slideDownEasing,
					onComplete : function() {
					}
				});
			} else {
				//animate slide up
				TweenMax.to($('#' + subNavIdName), opts.slideUpDuration, {
					css : {
						height : 0
					},
					ease : opts.slideUpEasing,
					onComplete : function() {
						//reset all subnavs
						$(".header-subnav-container").css("height", "0px");
						$(".header-subnav-container").css("z-index", "0");
						$(".header-subnav-container").css("padding", "0px");
						$(".header-subnav-container").css("border", "0px solid #cfcfcf");
						$(".arrow-icon").css("background-position", "0 0");
						TweenMax.killTweensOf($(".header-subnav-container"));

						//reset current ID
						currentID = "";
					}
				});
			}
		}

		//highlight MenuPoint
		function highlightMenuPoint(idName, hlBoolean) {
			if (hlBoolean) {
				$("#" + idName).addClass("nav-hover");
			} else {
				if (idName != currentID) {
					$("#" + idName).removeClass("nav-hover");
				}
			}
		}

	};
})(jQuery); 