/*
 ################################################################
 # ProductSlider - Jquerry-Plugin-File for home-view
 # Author: 		Matthias Rosenthal
 # Website:		http://www.matthiasrosenthal.de
 # Version 1.0:	2013/02/09
 ################################################################
 */
(function($) {
	$.fn.productSlider = function(options) {
		//set up default options
		var defaults = {
			startIndex : 1, //starting with image of start-index
			slideEasing : Cubic.easeOut, //easing method for the slider
			slideDuration : 0.5 //easing duration for the slider
		};

		//vars options
		var opts = $.extend({}, defaults, options);

		//find each product-slider
		return this.each(function() {
			//add mouse events
			$(this).find(".product-thumb").click(function() {
				var indexById = parseInt($(this).attr("id"));

				//show image and info-text-box
				showProduct(indexById);
			});

			//show entry by start-index
			showProduct(opts.startIndex);
		});

		//show image by index
		function showProduct(index) {
			//slide highlight-thumb
			slideThumb((index));

			//get image and text-infos by product-div in html-DOM
			var productDiv = $(".products").find("#product_" + index);

			var imageUrl = $(productDiv).attr("data-image-url")
			var subline = $(productDiv).find(".subline").html();
			var headline = $(productDiv).find(".headline").html();
			var price = $(productDiv).find(".price").html();
			var copy = $(productDiv).find(".copy").html();
			var linkLabel = $(productDiv).find(".product-link").html();
			var linkUrl = $(productDiv).find(".product-link").attr("href");

			//add infos
			$(".product-info-inner").find(".subline").html(subline);
			$(".product-info-inner").find(".headline").html(headline);
			$(".product-info-inner").find(".price").html(price);
			$(".product-info-inner").find(".copy").html(copy);
			$(".product-info-inner").find(".product-link").html(subline);
			$(".product-info-inner").find(".product-link").attr("href", linkUrl);

			var infoElementsArray = [$(".product-info-inner").find(".subline"), $(".product-info-inner").find(".headline"), $(".product-info-inner").find(".price"), $(".product-info-inner").find(".copy")];

			//load image ->
			$("#product_image_next img").attr("src", imageUrl);
			$("#product_image_next img").css("margin-left", "605px");
			
			//show image-container
			$("#product_image_next").css("display", "block");
			$("#product_image").css("display", "block");


			$("#product_image_next img").load(function() {
				//on load complete -> animation in (TBD: add preloader layout)
				TweenMax.killTweensOf($("#product_image_next img"));

				var marginLeftPos = 0;
				// check if browser is < IE9 -> set margin to 10px
				if (parseFloat(navigator.appVersion.split("MSIE")[1] < 9)) {
					marginLeftPos = 10;
				}

				TweenMax.to($("#product_image_next img"), opts.slideDuration, {
					css : {
						marginLeft : marginLeftPos
					},
					ease : opts.slideEasing,
					onComplete : function() {
						$("#product_image img").attr("src", imageUrl);
					}
				});
			})
			//animate all text elements in info box
			for (var i = 0; i < infoElementsArray.length; i++) {
				$(infoElementsArray[i]).css("margin-left", "700px");
				TweenMax.killTweensOf($(infoElementsArray[i]));
				TweenMax.to($(infoElementsArray[i]), 0.7 + 0.1 * i, {
					css : {
						marginLeft : 0
					},
					ease : opts.slideDuration
				});
			}

			//animate link
			TweenMax.killTweensOf($(".product-info-inner").find(".product-link"));
			TweenMax.to($(".product-info-inner").find(".product-link"), 0, {
				css : {
					autoAlpha : 0
				}
			});
			TweenMax.to($(".product-info-inner").find(".product-link"), 0.5, {
				css : {
					autoAlpha : 1
				}
			}).delay(1);
		};

		//animate thumb to x-position
		function slideThumb(index) {
			//calculation leftpositsion - 4px - border
			var leftPos = (index - 1) * 198 - 4;

			TweenMax.to($('.product_thumb_hl_rect'), opts.slideDuration, {
				css : {
					left : leftPos
				},
				ease : opts.slideEasing,
				onComplete : function() {
				}
			});
		}

	};
})(jQuery); 